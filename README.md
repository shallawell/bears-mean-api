## Synopsis
A MEAN-API to provide:
1. API to CRUD bear names
2. Added JSON web token authentication

## install nodejs, node package manager, mongoDB, download this git, then install this app
```
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-3.2.list
apt-get update
mkdir /data && mkdir /data/db
apt-get install -y nodejs npm 
apt-get install -y --allow-unauthenticated mongodb-org
git clone https://shallawell@gitlab.com/shallawell/bears-mean-api.git
mongod & 
cd bears-mean-api/ 
npm install nodemon 
npm install
ln -s "$(which nodejs)" /usr/bin/node
nodemon server.js
```
## Motivation
To further my personal software development skills and usage of tech such as MongoDB, Nodejs, Express framework, API development

## Testing using Postman 
| Route                  | HTTP Verb                               | Description       | Requires Auth Token |
| -----------------------|-----------------------------------------|-------------------|---------------------|
| `/setup` | `GET`    | sets up a default user bearAdmin to allow token creation via /api/auth/gettoken. | no |
| `/api/bears` | `GET`    | Get all the bears. | no |
| `/api/bears/:bear_id` | `GET`    | GET Get a single bear. | no |
| `/api/bears` | `POST Body=x-www-form-urlencoded, name=<bearname>, password=<bearpassword>`    | Create a bear | no |
| `/api/bears/:bear_id` | `DELETE`    | Delete a bear. | no |
| `/api/bears/:bear_id` | `PUT Body=x-www-form-urlencoded, name=<newbearname>`    | Update a bear with new name | no |
| `/api/auth/gettoken` | `POST Body=x-www-form-urlencoded, name=bearAdmin, password=bearPassword` | Creates a JSON web token for user bearAdmin | no |
| `/api/auth/users` | `GET Header=x-access-token, token=<JSONtoken>` | Get all the users | yes |

## TO DO
1. add token auth to 'non-GET' for /api/bears 
2. add more unit tests for each case
3. add error handling to stop it from crashing with null inputs etc
4. turn it into a SPA (Single Page App) by adding a AngularJS html GUI front end for User Auth, setup, add header+token, etc.
5. add Swagger API documentation

		
## Author
@shallawell

## License
MIT

## Acknowledgements
(https://scotch.io/tutorials/build-a-restful-api-using-node-and-express-4) <br>
(https://scotch.io/tutorials/authenticate-a-node-js-api-with-json-web-tokens)